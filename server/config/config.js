/**
 * Created by theotheu on 09-10-14.
 */
module.exports = {
    development: {
        debug: true,                           // set debugging on/off
        db: 'mongodb://localhost:27017/cria-dev',    // change with your database
        port: 8081                            // change 3000 with your port number
    }, test: {
        debug: true,                          // set debugging on/off
        db: 'mongodb://localhost:27017/cria-test',   // change with your database
        port: 8080                            // change 1300 with your port number
    }, production: {

    }
};