/*global require, unescape: true */
/**
 * Created by theotheu on 24-12-13.
 */
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    schema = mongoose.Schema;


/* Schema definitions */
// Schema types @see http://mongoosejs.com/docs/schematypes.html
var schemaName = schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    modificationDate: {type: Date, "default": Date.now}
}, {collection: "users"});

var modelName = "User";

module.exports = mongoose.model(modelName, schemaName);
