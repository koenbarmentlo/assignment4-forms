/**
 * Created by theotheu on 23-03-15.
 */
var mongoose = require('mongoose'),
    schema = mongoose.Schema;

var pricingSchema = schema({});

var imageSchema = schema({
    large: [{type: String}],
    thumb: [{type: String}],
    small: [{type: String}],
    normal: [{type: String}],
    zoom: [{type: String}]
});

var metricPackageSchema = schema({
    
});
var atchItem = schema({
    attachmentName: {type: String},
	articleNumber: {type: String},
	attachmentPath: {type:String}
});

var attachmentSchema = schema({
    type: {type: String},
    name: {type: String},
    atchArray: [atchItem]
});

var validDesignSchema = schema({
    // TODO: add the valid design properties
	// Not used and no properties
});

var metricPackageInfoSchema = schema({
	quantity: {type: String},
    length: {type: Number},
    width: {type: String},
    articleNumber: {type: String},
    weight: {type: Number},
    Heigth: {type: Number}
});

var itemSchema = schema({
    californiaTitle20Product: {type: Boolean},
    prices: [pricingSchema],
    images: [imageSchema],
    buyable: {type: Boolean},
    metric: {type: String},
    color: {type: String},
    custBenefit: {type: String},
    environment: {type: String},
    availabilityUrl: {type: String},
    packagePopupUrl: {type: String},
    url: {type: String},
    goodToKnow: {type: String},
    nopackages: {type: String},
    techInfoArr: [],
    careInst: {type: String},
    validDesign: [],
    goodToKnowPOP: {type: String},
    partNumber: {type: String},
    attachments: [attachmentSchema],
    bti: {type: Boolean},
    name: {type: String, require: true},
    soldSeparately: {type: String},
    reqAssembly: {type: Boolean},
    metricPackageInfo: [metricPackageInfoSchema],
    type: {type: String},
    dualCurrencies: {type: Boolean},
    catEntryId: {type: String},
    descriptiveAttributes: {type: Object},
    imperial: {type: String}
});

var attributeSchema = schema({
    type: {type: String, required: true},
    name: {type: String, required: true},
    id: {type: String, required: true}
});

var productSchema = schema({
        partNumber: {type: String, required: true},
        attributes: [attributeSchema],
        catEntryId: {type: String, required: true},
        items: [itemSchema]
    },
    {collection: "ikeaProducts"});

module.exports = mongoose.model('Product', productSchema);
